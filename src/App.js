import React from 'react';
import Contador from './components/contador';
import Saludo from './components/saludo';
import Temperatura from './components/temperatura';
import Listas from './components/listas';
import Form from './components/formulario';
function App() {
    return (
        <div>
            {/*
            <Contador />
            <Saludo msje='¿que talca?' />
            <Temperatura temp={10}></Temperatura>
             <Listas/>
             */}
            <Form />
        </div>
    );
}

export default App;
