import React, { Fragment, useState } from 'react';

const Form = () => {
    const [fruta, setFruta] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const guardarDatos = (e) => {
        e.preventDefault();

        if (!fruta.trim()) {
            console.log('esta vacio');
            return; //sale de la funcion
        }

        if (!descripcion.trim()) {
            console.log('esta vacio');
            return;
        }
        console.log(' procesando datos ' + fruta + ' ' + descripcion);

        e.target.reset(); //limpia lo campos
        setFruta('');
        setDescripcion('');
    };
    const initialList = [];
    const [lista, setLista] = useState(initialList);
    const addList = () => {
        setLista([...lista, { nombre: fruta, descripcion: descripcion }]);
    };
    return (
        <div className='container col-md-3'>
            <h1>Formulario</h1>
            <form onSubmit={guardarDatos}>
                <input
                    className='form-control mb-2'
                    type='text'
                    placeholder='ingrese fruta'
                    onChange={(e) => {
                        setFruta(e.target.value);
                    }}
                />
                <input
                    className='form-control mb-2'
                    type='text'
                    placeholder='ingrese descripcion'
                    onChange={(e) => {
                        setDescripcion(e.target.value);
                    }}
                />
                <button
                    type='submit'
                    className='btn btn-primary btn-block'
                    onClick={() => {
                        addList();
                    }}>
                    Agregar
                </button>
            </form>
            <hr />
            <h2>Lista de frutas</h2>
            <Fragment>
                {lista.map((item, index) => (
                    <ul>
                        <li key={index}>
                            {item.nombre}:{item.descripcion}
                        </li>
                    </ul>
                ))}
            </Fragment>
        </div>
    );
};
export default Form;
