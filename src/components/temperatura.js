import React, { Fragment } from 'react';

const Temperatura = (Props) => {
    return (
        <Fragment>
            <h2>Frio o calor?</h2>
            <p>{Props.temp > 20 ? 'Calor' : 'Frio!'}</p>
        </Fragment>
    );
};

export default Temperatura;
