import React, { useState } from 'react';

const Contador = () => {
    //[variable, metodo]
    const [count, setCount] = useState(0);
    const text = 'Cantidad de clicks';
    return (
        <div>
            <h3>
                {text} :{count}
            </h3>
            <h4>{count < 5 ? 'Es menor a 5' : 'Es mayor a 5'}</h4>
            <button onClick={() => setCount(count + 1)}>Sumar</button>
            <br />
        </div>
    );
};

export default Contador;
