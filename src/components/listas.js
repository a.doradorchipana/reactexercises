import React, { Fragment, useState } from 'react';

const Listas = () => {
    //const initialState = [1, 2, 3, 4, 5];
    const initialState = [
        { id: 1, text: 'tarea1' },
        { id: 2, text: 'tarea2' },
        { id: 3, text: 'tarea3' },
    ];
    const [lista, setItem] = useState(initialState);
    const agregarElemento = () => {
        //operador de propagacion
        setItem([...lista, { id: 4, text: 'tarea4' }]);
    };

    return (
        <Fragment>
            {/*cada vez que se use map se debe tener un key*/}
            {lista.map((item, index) => (
                <h4 key={index}>
                    {item.id}-{item.text}
                </h4>
            ))}
            <button onClick={() => agregarElemento()}>
                Agregar agregarElemento
            </button>
        </Fragment>
    );
};

export default Listas;
