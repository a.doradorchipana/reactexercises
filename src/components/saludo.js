import React from 'react';

const Saludo = (Props) => {
    const source =
        'https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/LutraCanadensis_fullres.jpg/250px-LutraCanadensis_fullres.jpg';
    return (
        <div>
            <h3>{Props.msje}</h3>
            <img src={source} alt='nutria' />
        </div>
    );
};

export default Saludo;
